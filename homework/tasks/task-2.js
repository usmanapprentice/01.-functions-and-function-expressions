/* Task description */
/*
	Write a function that finds all the prime numbers in a range
		1) it should return the prime numbers in an array
		2) it must throw an Error if any on the range params is not convertible to `Number`
		3) it must throw an Error if any of the range params is missing
// */
function IsPrime (num) {
	for (let i = 2; i <= Math.floor(num/2); i++) {
		if (num%i === 0) {
			return false;
		}
	}
	return true;
}

function solve() {
	return function findPrimes(Start, End) {
		let PrimeArray=[];
		if (Start == undefined || End == undefined) {
			throw '';
		}
		Start = +(Start);
		End = +(End);
		for (let i = Start; i <= End; i++) {
				if (IsPrime(i)==true && i > 1) {
					PrimeArray.push(i);
				}
			}
		return PrimeArray;
	}
}

module.exports = solve;
