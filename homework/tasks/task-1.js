/* Task Description */
/* 
	Write a function that sums an array of numbers:
		numbers must be always of type Number
		returns `null` if the array is empty
		throws Error if the parameter is not passed (undefined)
		throws if any of the elements is not convertible to Number	

*/

function solve() {
	return function(arr) {
		let sum = 0;
		if (arr.some(x => Number.isNaN(Number(x)))) {
			throw 'not all elements are numbers';
		}
		if (arr.length == 0 ) {
			return null;
		}
		for (let i = 0; i < arr.length; i++) {
			sum += (+arr[i]);
		}
		return sum;
	}
}

module.exports = solve;
